# Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float.
# Якщо перетворити не вдається функція має повернути 0.
tsk1 = '---------------------------------------- Task 1 ------------------------------------------------'
space = '------------------------------------------------------------------------------------------------'
def convert_to_float(value):
    try:
        float_value = float(value)
        return float_value
    except:
        return 0
my_input = input("Enter please your data.\n")
result = convert_to_float(my_input)
print(space)
print(f'Your result is ----> {result}\nType your result is ---->', (type(result)))
print(space)

